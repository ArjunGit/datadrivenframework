package com.mindtree.vibgyorpaints.interfaces;

import java.util.ArrayList;

public interface LocatingStoreInterface
{
	
	public ArrayList<String> getLocatingStoreAction();
	public ArrayList<String> getLocatingStoreLocator();

}
