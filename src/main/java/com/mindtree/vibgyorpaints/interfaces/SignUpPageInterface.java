package com.mindtree.vibgyorpaints.interfaces;

import java.util.ArrayList;

import com.mindtree.vibgyorpaints.exceptions.CannotFindFileException;
import com.mindtree.vibgyorpaints.exceptions.FormatNotValidException;
import com.mindtree.vibgyorpaints.exceptions.InputOutputException;

public interface SignUpPageInterface 
{
	
	public ArrayList<String> getSignUpAction() throws CannotFindFileException, FormatNotValidException, InputOutputException;
	public ArrayList<String> getSignUpLocator();

}
