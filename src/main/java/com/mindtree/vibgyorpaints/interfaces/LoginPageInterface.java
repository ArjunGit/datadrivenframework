package com.mindtree.vibgyorpaints.interfaces;

import java.util.ArrayList;

import com.mindtree.vibgyorpaints.exceptions.CannotFindFileException;
import com.mindtree.vibgyorpaints.exceptions.FormatNotValidException;
import com.mindtree.vibgyorpaints.exceptions.InputOutputException;

public interface LoginPageInterface 
{

	public ArrayList<String> getLoginAction() throws CannotFindFileException, FormatNotValidException, InputOutputException;
	public ArrayList<String> getLoginLocator();
}
