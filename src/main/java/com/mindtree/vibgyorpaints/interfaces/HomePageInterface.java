package com.mindtree.vibgyorpaints.interfaces;

import java.util.ArrayList;

public interface HomePageInterface 
{
	public ArrayList<String> getHomepageAction();
	public ArrayList<String> getHomePageLocator();

}
