package com.mindtree.vibgyorpaints.locators;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.mindtree.vibgyorpaints.base.TestBase;

public class SignUp extends TestBase
{

	@FindBy(xpath = "//ul[@class='navbar-nav ml-auto']//li[2]")
	WebElement clickSignup;
	
	@FindBy(id = "emailId")
	WebElement emailId;
	
	@FindBy(id = "password")
	WebElement password;
	
	@FindBy(id = "ConfirmPassword")
	WebElement confirmPassword;
	
	@FindBy(id = "Fullname")
	WebElement userName;
	
	@FindBy(id = "Location")
	WebElement location;
	
	@FindBy(id = "MobileNo")
	WebElement phoneNumber;
	
	@FindBy(xpath = "//input[@class='btn btn-success']")
	WebElement signUp;
	
	
	
	public  SignUp() {
		PageFactory.initElements(driver, this);
	}
	

	public void signUp(String email,String pass,String cpwd,String username,String loc,String phn)
	{
		clickSignup.click();
		System.out.println(email);
		emailId.sendKeys(email);
		password.sendKeys(pass);
		confirmPassword.sendKeys(cpwd);
		userName.sendKeys(username);
		location.sendKeys(loc);
		phoneNumber.sendKeys(phn);
		signUp.click();
	}
	
}
