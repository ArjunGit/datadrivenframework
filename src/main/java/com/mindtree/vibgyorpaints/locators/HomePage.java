package com.mindtree.vibgyorpaints.locators;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.mindtree.vibgyorpaints.base.TestBase;
import com.mindtree.vibgyorpaints.exceptions.LocatorNotFound;

public class HomePage extends TestBase
{

	@FindBy(xpath = "//a[text()='VIBGYOR PAINTS']")
	WebElement vibgyorPaintLabel;

	@FindBy(xpath = "//a[text()='Login']")
	WebElement loginLink;

	@FindBy(xpath = "//a[text()='Signup']")
	WebElement signUpLink;

	public HomePage() 
	{
		
		PageFactory.initElements(driver, this);
			
	}

	public String verifyHomePageTitle()
	{
		return driver.getTitle();
	}
	
	public boolean verifyvibgyorPaintLabel() throws LocatorNotFound
	{
		try {
		return vibgyorPaintLabel.isDisplayed();
		}
		catch (Exception e) 
		{
			throw new LocatorNotFound("Locator not found");
			
		}
	}
	
	public boolean verifyloginLabel() throws LocatorNotFound
	{
		try
		{
		return loginLink.isDisplayed();
		}
		catch (Exception e) {
			throw new LocatorNotFound("Locator not found");
		}
	}
	
	public boolean verifysignUpLabel()
	{
		return signUpLink.isDisplayed();
	}
	
	public LoginPage clickOnLoginLink()
	{
		loginLink.click();
		return new LoginPage();
	}

}
