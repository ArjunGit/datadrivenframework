package com.mindtree.vibgyorpaints.locators;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import com.mindtree.vibgyorpaints.base.TestBase;
import com.mindtree.vibgyorpaints.utils.TestUtil;

public class LoginPage extends TestBase
{
	By login=By.xpath("//a[text()='Login']");

	By emailid= By.xpath("//*[@id='EmailId']");
	
	By password=By.id("Password");
	
	By loginbuton=By.xpath("//input[@value='Login']");
	
	By textValue=By.id("swal2-content");

	By clickOk=By.xpath("//button[text()='OK']");
	

	public LoginPage(){
		PageFactory.initElements(driver, this);
	}

	public String login(String email, String pwd) throws InterruptedException
	{
		
		driver.findElement(login).click();
		Thread.sleep(2000);
		driver.findElement(emailid).sendKeys(email);
		driver.findElement(password).sendKeys(pwd);
		driver.findElement(loginbuton).click();
		String str=driver.findElement(By.xpath("//div[@id='swal2-content']")).getText();
		driver.manage().timeouts().implicitlyWait(TestUtil.IMPLICIT_WAIT, TimeUnit.SECONDS); 
        //System.err.println(str);
		return str;

	}

}
