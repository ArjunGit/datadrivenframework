package com.mindtree.vibgyorpaints.exceptions;


public class LocatorNotFound extends Exception
{
	public LocatorNotFound(String s) {
		super(s);
	}

}
