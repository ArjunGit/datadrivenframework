package com.mindtree.vibgyorpaints.utils;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class ScreenShot {
	
	public static String captureScreenShot(WebDriver driver,String screenShotName)
	{
		try {
TakesScreenshot ts = (TakesScreenshot) driver;
			
			File src = ts.getScreenshotAs(OutputType.FILE);
			String dest = "D:\\SelinumWorkSpace\\VibgyorPaints\\snap"+screenShotName+System.currentTimeMillis()+".png";
			File destination = new File(dest);
			FileUtils.copyFile(src, destination);
			
			return dest;
		}
		catch (Exception e) {
			
			System.out.println("Exception while taking screenshots "+e.getMessage());
			return e.getMessage();
		}
		//return screenShotName;
	}

}
