package com.mindtree.vibgyorpaints.imp;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.mindtree.vibgyorpaints.base.TestBase;
import com.mindtree.vibgyorpaints.interfaces.LocatingStoreInterface;
import com.mindtree.vibgyorpaints.locators.*;

public class LocatingStores extends TestBase implements LocatingStoreInterface
{
	String sheetname="Sheet1";

	public ArrayList<String> getLocatingStoreAction()
	{
		ArrayList<String> actions=LocatingStoresPageLocator.getTestData(sheetname);
		return actions;
	}
	
	public ArrayList<String> getLocatingStoreLocator(){
		ArrayList<String> locator=new ArrayList<String>();
		locator.add(LocatingStoresPageLocator.locatingStore);
		locator.add(LocatingStoresPageLocator.bangaloreStore);
		locator.add(LocatingStoresPageLocator.storeInBangalore);
		
		locator.add(LocatingStoresPageLocator.locatingStore);
		locator.add(LocatingStoresPageLocator.hyderabadStore);
		locator.add(LocatingStoresPageLocator.storeInHyderabad);
		
		locator.add(LocatingStoresPageLocator.locatingStore);
		locator.add(LocatingStoresPageLocator.mumbaiStore);
		locator.add(LocatingStoresPageLocator.storeInMumbai);
		
		locator.add(LocatingStoresPageLocator.locatingStore);
		locator.add(LocatingStoresPageLocator.chennaiStore);
		locator.add(LocatingStoresPageLocator.storeInChennai);
		
		
		locator.add(LocatingStoresPageLocator.locatingStore);
		locator.add(LocatingStoresPageLocator.kolkataStore);
		locator.add(LocatingStoresPageLocator.storeInKolkata);
		return locator;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	WebDriverWait wait=new WebDriverWait(driver, 30);
	By locatingStore;

	By bangaloreStore;
	
	By hyderabadStore;
	
	By mumbaiStore;
	
	By chennaiStore;
	
	By kolkataStore;

	By storeInBangalore;
	
	By storeInHyderabad;
	
	By storeInMumbai;
	
	By storeInChennai;
	
	By storeInKolkata;
	
	
	 public LocatingStores()
	{
		PageFactory.initElements(driver, this);
		
	}
	 
	 
	 
	 public void setLocaters() 
		{
			LocatingStoresPageLocator.getTestData("Sheet1");
			locatingStore=By.xpath(LocatingStoresPageLocator.locatingStore);
			//System.err.println(locatingStore);
			
			bangaloreStore=By.xpath(LocatingStoresPageLocator.bangaloreStore);
			//System.err.println(bangaloreStore);
			storeInBangalore=By.xpath(LocatingStoresPageLocator.storeInBangalore);
			//System.err.println(storeInBangalore);
		    
			
		    mumbaiStore=By.xpath(LocatingStoresPageLocator.mumbaiStore);
		    //System.err.println(mumbaiStore);
		    
		    storeInMumbai=By.xpath(LocatingStoresPageLocator.storeInMumbai);
		    //System.err.println(storeInMumbai);
		    
			chennaiStore=By.xpath(LocatingStoresPageLocator.chennaiStore);
			storeInChennai=By.xpath(LocatingStoresPageLocator.storeInChennai);
			
			kolkataStore=By.xpath(LocatingStoresPageLocator.kolkataStore);
			storeInKolkata=By.xpath(LocatingStoresPageLocator.storeInKolkata);
			//System.err.println(storeInKolkata);
			
			hyderabadStore=By.xpath(LocatingStoresPageLocator.hyderabadStore);
		    //System.err.println(hyderabadStore);
			storeInHyderabad=By.xpath(LocatingStoresPageLocator.storeInHyderabad);
			
			
			
			
			
					}
	
	 public boolean locatingStoreIcon()
	 {
		 boolean res=driver.findElement(locatingStore).isDisplayed();
		 return res;
	 }
	 
	 public String validateBangalore()
	 {
		 String storeName;
		 driver.findElement(locatingStore).click();
		 
		 wait.until(ExpectedConditions.visibilityOfElementLocated(bangaloreStore)).click();
		 storeName=wait.until(ExpectedConditions.visibilityOfElementLocated(storeInBangalore)).getText();
		 return  storeName;
	 }
	 
	 public String validateHyderabad()
	 {
		 String storeName;
		 driver.findElement(locatingStore).click();
		 
		 wait.until(ExpectedConditions.visibilityOfElementLocated(hyderabadStore)).click();
		 storeName=wait.until(ExpectedConditions.visibilityOfElementLocated(storeInHyderabad)).getText();
		 return  storeName;
	 }
	 
	 public String validateMumbai()
	 {
		 String storeName;
		 driver.findElement(locatingStore).click();
		 
		 wait.until(ExpectedConditions.visibilityOfElementLocated(mumbaiStore)).click();
		 storeName=wait.until(ExpectedConditions.visibilityOfElementLocated(storeInMumbai)).getText();
		 return  storeName;
	 }
	 
	 public String validateChennai()
	 {
		 String storeName;
		 driver.findElement(locatingStore).click();
		 
		 wait.until(ExpectedConditions.visibilityOfElementLocated(chennaiStore)).click();
		 storeName=wait.until(ExpectedConditions.visibilityOfElementLocated(storeInChennai)).getText();
		 return  storeName;
	 }
	 
	 public String validateKolkata()
	 {
		 String storeName;
		 driver.findElement(locatingStore).click();
		 
		 wait.until(ExpectedConditions.visibilityOfElementLocated(kolkataStore)).click();
		 storeName=wait.until(ExpectedConditions.visibilityOfElementLocated(storeInKolkata)).getText();
		 return  storeName;
	 }

}
