package com.mindtree.vibgyorpaints.imp;




import java.util.ArrayList;

import org.openqa.selenium.By;
import com.mindtree.vibgyorpaints.base.TestBase;
import com.mindtree.vibgyorpaints.interfaces.HomePageInterface;
import com.mindtree.vibgyorpaints.locators.*;

public class HomePage extends TestBase implements HomePageInterface
{
	By vibgyorPaintLabel;
	By loginLabel;
	By signUpLabel;
	String sheetname="Sheet1";
	
	public ArrayList<String> getHomepageAction()
	{
		ArrayList<String> actions=HomePageLocators.getTestData(sheetname);
		return actions;
	}
	
	public ArrayList<String> getHomePageLocator()
	{
		ArrayList<String> locator=new ArrayList<String>();
		locator.add(HomePageLocators.vibgyorPaintLabel);
		locator.add(HomePageLocators.loginLabel);
		locator.add(HomePageLocators.signUpLabel);
		return locator;
	}
	

	

	public String verifyHomePageTitle()
	{
		return driver.getTitle();
	}
	
	public boolean verifyvibgyorPaintLabel()
	{
		return  driver.findElement(vibgyorPaintLabel).isDisplayed();
	}
	
	public boolean verifyloginLabel()
	{
		return driver.findElement(loginLabel).isDisplayed();
	}
	
	public boolean verifysignUpLabel()
	{
		return driver.findElement(signUpLabel).isDisplayed();
	}
}
