package com.mindtree.vibgyorpaints.testcases;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


import com.mindtree.vibgyorpaints.base.TestBase;
import com.mindtree.vibgyorpaints.locators.SignUp;
import com.mindtree.vibgyorpaints.utils.TestUtil;

public class SignupPageTest extends TestBase {
	String sheetName="SignUp";
	SignUp signup;

	public SignupPageTest() {
		super();
		
	}
	
	@BeforeMethod
	public void setUp(){
		initialization();
		signup=new SignUp();
	}
	
	
	
	
	@DataProvider(name="SignUpTestData")
	public Object[][] getTestData(){
		Object data[][] = TestUtil.getTestData(sheetName);
		return data;
	}
	
	@Test(dataProvider = "SignUpTestData")
	public void SignUpTest(String emailId, String password,String confirmPassword,String userName,String location,String phoneNo)
	{
		signup.signUp(emailId, password, confirmPassword, userName, location, phoneNo);
	}
	
	@AfterMethod()
	void teardown() throws InterruptedException
	{
		Thread.sleep(2000);
		driver.close();
	}
	
		


}
