package com.mindtree.vibgyorpaints.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.mindtree.vibgyorpaints.base.TestBase;
import com.mindtree.vibgyorpaints.locators.LoginPage;
import com.mindtree.vibgyorpaints.utils.TestUtil;

public class LoginPageTest extends TestBase
{
	LoginPage loginpage;
	String sheetName = "Login";

	public LoginPageTest() 
	{
	
		super();
		
	}

	@BeforeMethod
	public void setUp()
	{
		initialization();
		loginpage = new LoginPage();	
	}

	@DataProvider(name="VibgyorpaintTestData")
	public Object[][] getTestData(){
		Object data[][] = TestUtil.getTestData(sheetName);
		return data;
	}

	@Test(priority=1, dataProvider="VibgyorpaintTestData")
	public void loginPageTest(String email, String pwd) throws InterruptedException
	{
		//System.out.println(email+"   "+pwd);
		//String value=
		String actualValue=loginpage.login(email, pwd);

		Assert.assertEquals(actualValue,"login successful","login not sucessfull");


	}



	@AfterMethod
	public void tearDown() throws InterruptedException{
		Thread.sleep(3000);
		driver.quit();
	}
}
