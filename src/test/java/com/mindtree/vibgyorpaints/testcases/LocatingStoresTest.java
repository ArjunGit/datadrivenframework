package com.mindtree.vibgyorpaints.testcases;

import java.util.ArrayList;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.mindtree.vibgyorpaints.base.TestBase;
import com.mindtree.vibgyorpaints.exceptions.ElementNotClickable;
import com.mindtree.vibgyorpaints.exceptions.ElementNotPresentException;
import com.mindtree.vibgyorpaints.exceptions.LocatorNotFound;

import com.mindtree.vibgyorpaints.imp.LocatingStores;
import com.mindtree.vibgyorpaints.utils.CommonMethod;
import com.mindtree.vibgyorpaints.utils.ScreenShot;
import com.mindtree.vibgyorpaints.utils.TestUtil;
import com.relevantcodes.extentreports.LogStatus;

public class LocatingStoresTest extends TestBase {

	LocatingStores location;
	String sheetName = "paint";
	ArrayList<String> al;
	ArrayList<String> kk;
	static int i = -1;
	int flag;

	public LocatingStoresTest() {
		super();

	}

	@BeforeMethod
	public void setUp() {
		initialization();
		location = new LocatingStores();
		flag = 0;
		++i;
		

	}

	@DataProvider(name = "VibgyorpaintTestData")
	public Object[][] getTestData() {
		Object data[][] = TestUtil.getTestData(sheetName);
		return data;
	}

	@Test(priority = 3, dataProvider = "VibgyorpaintTestData")
	public void locatingStoreTest(String paintStore) throws InterruptedException {
		try {
			String actualValue = null;
			logger = report.startTest("Locating store page test");
			log.info("Locating store page test");
			logger.log(LogStatus.INFO, "Verifying paint stores");
			log.info("Verifying Paint Stores");

			al = location.getLocatingStoreAction();

			kk = location.getLocatingStoreLocator();

			for (; i < al.size(); i++) {
				switch (al.get(i)) {
				case "click":
					CommonMethod.click_element(kk.get(i));
					break;
				case "islabelpresent":
					Assert.assertTrue(CommonMethod.islabelpresent(kk.get(i)));
					logger.log(LogStatus.PASS, "Label is Present");
					log.info("Label is Present");
					break;
				case "gettext":
					actualValue = CommonMethod.gettext(kk.get(i)); {
					flag = 1;
				}
					Assert.assertEquals(actualValue, paintStore, "No stores present");
					logger.log(LogStatus.PASS, "Stores found at the location");
					log.info("Stores found at the location");
					break;

				}

				if (flag == 1) {

					break;
				}

			}
		} catch (ElementNotClickable e) {
			log.error(e.getMessage());
		} catch (ElementNotPresentException e) {
			log.error(e.getMessage());
		} catch (LocatorNotFound e) {
			log.error("unable to find element");
		} catch (Exception e) {
			log.fatal(e.getMessage());
		}

	}

	@AfterMethod()
	void teardown(ITestResult result) {
		if (result.getStatus() == ITestResult.FAILURE) {
			String imagePath = ScreenShot.captureScreenShot(driver, result.getName());
			String image = logger.addScreenCapture(imagePath) + "Some error occurred in Locating Store";
			logger.log(LogStatus.ERROR, image);

		}
		report.endTest(logger);
		report.flush();

		driver.close();
	}

}
