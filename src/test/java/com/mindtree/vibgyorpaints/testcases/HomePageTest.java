package com.mindtree.vibgyorpaints.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.mindtree.vibgyorpaints.base.TestBase;
import com.mindtree.vibgyorpaints.exceptions.LocatorNotFound;
import com.mindtree.vibgyorpaints.locators.HomePage;

public class HomePageTest extends TestBase 
{
	HomePage homepage;
	public HomePageTest() 
	{
		super();
		
	}
	@BeforeMethod
	public void setUp(){
		initialization();
		homepage = new HomePage();	
	}
	@Test(priority=1)
	public void HomePageTitleTest()
	{
		String title = homepage.verifyHomePageTitle();
		Assert.assertEquals(title, "VibgyorFeb18Client","title of home page not matched");
	}
	
	@Test(priority=2)
	public void vibgyorpaintlabelTest()
	{
		try
		{
		Assert.assertTrue(homepage.verifyvibgyorPaintLabel());
		}
		catch (LocatorNotFound e) {
			System.out.println("locator not found");
		}
	}
	@Test(priority=3)
	public void loginlabelTest() 
	{
		
		try {
			Assert.assertTrue(homepage.verifyloginLabel());
		}
		catch (LocatorNotFound e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	@Test(priority=4)
	public void signuplabelTest()
	{
		Assert.assertTrue(homepage.verifysignUpLabel());
	}
	
	@AfterMethod
	public void tearDown()
	{
		driver.quit();
	}


}
